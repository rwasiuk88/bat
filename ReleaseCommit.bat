@echo off
setlocal EnableDelayedExpansion
set TEMP_SCRIPT_FILENAME=ReleaseCommitScript.tmp.bat
set PRODUCT_NAME=TFS4JIRA

REM ++++++++++++++++++++++++++
REM Copy and run current script from temp folder

call :assert_script_is_not_temp_file
if %ERRORLEVEL% == 0 (
    echo Copying scripts to a temp folder
    copy "%~f0" "%TEMP%\%TEMP_SCRIPT_FILENAME%"
    copy "VersionsSet.bat" "%TEMP%\VersionsSet.bat"
    echo Calling script again, but this time from a temp folder...
    call "%TEMP%\%TEMP_SCRIPT_FILENAME%" %* & exit /b 0
    exit /b 0
)

REM ++++++++++++++++++++++++++
REM intro

echo.
echo This is Release committer for %PRODUCT_NAME%
echo.

set SHOULD_PUSH=false
if "%~1" == "--push" (
    set SHOULD_PUSH=true
)
if "%SHOULD_PUSH%" == "true" (
    echo SHOULD_PUSH=true - Will push changes to remote repository 
)

REM ++++++++++++++++++++++++++
REM Check working directory

set IS_WORKING_DIRECTORY_CLEAN=true
for /f "delims=" %%l in ('git status --branch --porcelain') do (
    set line=%%l
    rem echo line: "!line!"
    if "!line:~0,3!" == "## " (
        call :deal_with_branch_line "!line!"
    ) else (
        set IS_WORKING_DIRECTORY_CLEAN=false
    )
)

if "%IS_WORKING_DIRECTORY_CLEAN%" == "false" (
    echo.
    echo ******************************************************************
    echo * Curent branch: %INITIAL_BRANCH_NAME%
    echo * Working directory is not clean^^!
    echo * Do you want to stash all changes and continue releasing?
    echo ******************************************************************
    choice /C:CA /M "Press C to Continue or A to Abort the script..."
    if ERRORLEVEL 2 goto aborting
    if ERRORLEVEL 1 echo proceeding...
    echo Stashing not commited changes  
    call git stash --include-untracked
    set ARE_LOCAL_CHANGES_STASHED=true
)

REM ++++++++++++++++++++++++++
REM Set new versions

:set_release_version
echo.
echo **********************************************
echo * Set new %PRODUCT_NAME% version to release
set /P RELEASE_VERSION=* Version number: 

echo.
echo **************************************
echo * Plugin version: %RELEASE_VERSION%
echo * Synchronizer version: %RELEASE_VERSION%
echo **************************************
choice /C:CRA /M "Press C to Continue, R to Retype or A to Abort the script..."
if ERRORLEVEL 3 goto aborting
if ERRORLEVEL 2 goto set_release_version
if ERRORLEVEL 1 echo proceeding...

REM ++++++++++++++++++++++++++
REM Make sure this version is not already released

git fetch --all

call :assert_command_has_no_response "git tag -l %RELEASE_VERSION%"
if ERRORLEVEL 1 (
	echo.
	echo ERROR: Tag "%RELEASE_VERSION%" already exists in repository. Make sure you have set proper release version!
	exit /b 1
)

REM ++++++++++++++++++++++++++
REM Checkout develop branch

git checkout develop
git pull || goto error_occured

REM ++++++++++++++++++++++++++
REM Update versions in pom.xml

call "%TEMP%\VersionsSet.bat" /Q %RELEASE_VERSION% %RELEASE_VERSION% || goto error_occured

REM ++++++++++++++++++++++++++
REM Commit versions to develop branch

git add -u
git commit -m "Release %PRODUCT_NAME% %RELEASE_VERSION%"
if "%SHOULD_PUSH%" == "true" (
    git push || goto error_occured
)

echo.
echo ******************************************************
if "%SHOULD_PUSH%" == "true" (
    echo * Versions updated, commited and pushed to 'develop' branch.
) else (
    echo * Versions updated and commited to 'develop' branch.
)
echo * Do you want to merge it to master?
echo ******************************************************
choice /C:CA /M "Press C to Continue or A to Abort the script..."
if ERRORLEVEL 2 goto aborting
if ERRORLEVEL 1 echo proceeding...

REM ++++++++++++++++++++++++++
REM Checkout master and merge develop

git checkout master
git pull || goto error_occured
git merge --no-commit --no-ff develop
git commit -m "Merge branch 'develop' into master due to release %RELEASE_VERSION%"
if "%SHOULD_PUSH%" == "true" (
    git push || goto error_occured
)

echo.
echo *********************************************************************************
echo * Branch 'develop' merged to master
echo *
if "%SHOULD_PUSH%" == "true" (
    echo * Now you should wait for Bamboo build, upload artifacts to Marketplace etc.
) else (
    echo * You should push commited changes now, then wait for Bamboo build, upload artifacts to Marketplace etc.
)
echo *
echo * If you're sure everyting works fine, you can continue
echo * ("%RELEASE_VERSION%" tag will be added to the current commit on master branch) 
echo *********************************************************************************
choice /C:CA /M "Press C to Continue or A to Abort the script..."
if ERRORLEVEL 2 goto aborting
if ERRORLEVEL 1 echo proceeding...

REM ++++++++++++++++++++++++++
REM Add tag on master

git tag "%RELEASE_VERSION%"
if "%SHOULD_PUSH%" == "true" (
    git push origin "%RELEASE_VERSION%"
)

REM ++++++++++++++++++++++++++
REM Rebase branch 'develop'

git checkout develop
git reset --hard master

REM ++++++++++++++++++++++++++
REM Prepare branch 'develop' to next development cycle

:set_next_versions
call :generate_next_versions "%RELEASE_VERSION%"

echo.
echo **************************************
echo * You've released version %RELEASE_VERSION%
echo *
echo * Set new %PRODUCT_NAME% versions for next development cycle
set /P NEXT_PLUGIN_VERSION=* Next plugin version [%NEXT_PLUGIN_VERSION%]: 
set /P NEXT_SYNCHRONIZER_VERSION=* Next Synchronizer version [%NEXT_SYNCHRONIZER_VERSION%]: 

echo.
echo **************************************
echo * Plugin version: %NEXT_PLUGIN_VERSION%
echo * Synchronizer version: %NEXT_SYNCHRONIZER_VERSION%
echo **************************************
choice /C:CRA /M "Press C to Continue, R to Retype or A to Abort the script..."
if ERRORLEVEL 3 goto aborting
if ERRORLEVEL 2 goto set_next_versions
if ERRORLEVEL 1 echo proceeding...

REM ++++++++++++++++++++++++++
REM Update versions in pom.xml

call "%TEMP%\VersionsSet.bat" /Q %NEXT_PLUGIN_VERSION% %NEXT_SYNCHRONIZER_VERSION || goto error_occured

REM ++++++++++++++++++++++++++
REM Commit new versions to develop branch

git add -u
git commit -m "Prepare for next development cycle" 
if "%SHOULD_PUSH%" == "true" (
    git push || goto error_occured
)

echo.
echo **************************************
if "%SHOULD_PUSH%" == "true" (
    echo * Versions updated, commited and pushed to 'develop' branch.
) else (
    echo * Versions updated and commited to 'develop' branch.
)
echo **************************************

if "%ARE_LOCAL_CHANGES_STASHED%" == "true" (
    echo.
    echo *********************************************************
    echo * You have stashed some changes at the beginning 
    choice /M "* Do you want to checkout '%INITIAL_BRANCH_NAME%' branch and unstash your previous changes?"
    if ERRORLEVEL 2 goto done
    if ERRORLEVEL 1 echo proceeding...
    git checkout %INITIAL_BRANCH_NAME%
    git stash pop
)
set ARE_LOCAL_CHANGES_STASHED=false

:done
echo.
echo.
echo =================
echo =    Done :)    =
echo =================
echo.


goto :eof
REM ==========================

:aborting
echo Aborting the script.
goto :eof

:deal_with_branch_line
for /f "tokens=2,3 delims=. " %%i in ("%~1") do (
    call :save_current_branch_name "%%i"
)
goto :eof

:save_current_branch_name
set INITIAL_BRANCH_NAME="%~1"
goto :eof

:assert_command_has_no_response
for /f "delims=" %%l in ('%~1') do (
    exit /b 1
)
exit /b 0
goto :eof

:assert_script_is_not_temp_file
    if "%~nx0" EQU "%TEMP_SCRIPT_FILENAME%" (
        exit /b 1
    ) else (
        exit /b 0
    )
goto :eof

:generate_next_versions
for /f "tokens=1-4 delims=." %%a in ("%~1") do (
	set versionParts[0]=%%a
	set versionParts[1]=%%b
	set versionParts[2]=%%c
	set versionParts[3]=%%d
)
for /l %%i in (0,1,3) do (
	if "!versionParts[%%i]!" == "" set versionParts[%%i]=0
	set /a versionPartsIncremented[%%i]=!versionParts[%%i]!+1
)
set NEXT_PLUGIN_VERSION=%versionParts[0]%.%versionParts[1]%.%versionPartsIncremented[2]%-SNAPSHOT
set NEXT_SYNCHRONIZER_VERSION=%versionParts[0]%.%versionParts[1]%.%versionParts[2]%.%versionPartsIncremented[3]%
goto :eof

:error_occured
echo ERROR: An error occured during script. See the logs above to findout what was the problem. 1>&2   
exit /b 1
goto :eof