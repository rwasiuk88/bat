@echo off
echo This is version updater for TFS4JIRA
echo.

if /i "%1" == "/Q" (
    set IS_IN_QUIET_MODE=true
    shift
) else (
    set IS_IN_QUIET_MODE=false
)

if [%1] == [] (
    echo You have to pass new version numbers as parameters
    echo Usage: VersionsSet.bat ^<plugin-version^> [^<synchronizer-version^>]
    exit /B 1
)

set PLUGIN_VERSION=%1
set SYNCHRONIZER_VERSION=%2

if [%2] == [] (
    echo Second parameter not set. Using Synchronizer version the same as Plugin version
    echo.
    set SYNCHRONIZER_VERSION=%PLUGIN_VERSION%
)

echo **************************************
echo Plugin version: %PLUGIN_VERSION%
echo Synchronizer version: %SYNCHRONIZER_VERSION%
echo **************************************

if NOT "%IS_IN_QUIET_MODE%" == "true" (
    choice /M "Do you want to proceed?"
    if ERRORLEVEL 2 goto canceling
    if ERRORLEVEL 1 echo proceeding...
)

set MAVEN_PATH=
for /f "delims=" %%i in ('where mvn') do set MAVEN_PATH="%%i"
if NOT [%MAVEN_PATH%] == [] GOTO run-script
for /f "delims=" %%i in ('where atlas-mvn') do set MAVEN_PATH="%%i"
if NOT [%MAVEN_PATH%] == [] GOTO run-script
echo Maven not found!
exit /B 1

:run-script
echo Maven path is: %MAVEN_PATH%

echo ++++++++++++++++++++++++++
call %MAVEN_PATH% -Pversions-set versions:set-property -Dproperty=synchronizer.version -DnewVersion="%SYNCHRONIZER_VERSION%"
echo ++++++++++++++++++++++++++
call %MAVEN_PATH% -Pversions-set versions:set -DnewVersion="%PLUGIN_VERSION%"
echo ++++++++++++++++++++++++++
call %MAVEN_PATH% -Pversions-set versions:commit
echo ++++++++++++++++++++++++++
echo Done.

goto :eof

:canceling
echo Canceling script.
goto :eof
